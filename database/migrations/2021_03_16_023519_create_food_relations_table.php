<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFoodRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_relations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('paket_id');
            $table->unsignedBigInteger('makanan_id');
            $table->timestamps();

            $table->foreign('paket_id')->references('id')->on('packages');
            $table->foreign('makanan_id')->references('id')->on('foods');
            $table->softDeletes();
        });

        $inserted = [array(
            'paket_id' => 1,
            'makanan_id' => 2,
        ), array(
            'paket_id' => 1,
            'makanan_id' => 2,
        ), array(
            'paket_id' => 2,
            'makanan_id' => 2,
        ), array(
            'paket_id' => 2,
            'makanan_id' => 3,
        ), array(
            'paket_id' => 3,
            'makanan_id' => 3,
        ), array(
            'paket_id' => 3,
            'makanan_id' => 4,
        ), array(
            'paket_id' => 4,
            'makanan_id' => 4,
        ), array(
            'paket_id' => 4,
            'makanan_id' => 5,
        )];
        DB::table('food_relations')->insert($inserted);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_relations');
    }
}
