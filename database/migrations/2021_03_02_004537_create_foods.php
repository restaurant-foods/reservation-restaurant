<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateFoods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_makanan');
            $table->string('harga_makanan');
            $table->string('keterangan_makanan');
            $table->string('image');
            // $table->unsignedBigInteger('paket_id');
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('paket_id')->references('id')->on('packages');
        });

        $inserted = [array(
            'nama_makanan' => 'Nasi Wagyu Kuah Woku',
            'harga_makanan' => '40000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/nasi-wagyu-kuah-woku.jpg',
        ), array(
            'nama_makanan' => 'Sop Pindang Kakap',
            'harga_makanan' => '25000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/sop-pindang-kakap.jpg',
        ), array(
            'nama_makanan' => 'Sosis Solo',
            'harga_makanan' => '15000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/sosis-solo.jpg',
        ), array(
            'nama_makanan' => 'Talua Barenda',
            'harga_makanan' => '25000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/talua-barenda.jpg',
        ), array(
            'nama_makanan' => 'Nasi Bali',
            'harga_makanan' => '20000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/nasi-bali.jpg',
        ), array(
            'nama_makanan' => 'Nasi Kapau',
            'harga_makanan' => '25000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/nasi-kapau.jpg',
        ), array(
            'nama_makanan' => 'Nasi Ayam Taliwang',
            'harga_makanan' => '15000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/nasi-ayam-taliwang.jpg',
        ), array(
            'nama_makanan' => 'Nasi Sate Wagyu',
            'harga_makanan' => '25000',
            'keterangan_makanan' => 'Bisa Pedas, Manis',
            'image' => 'foodsDefault/nasi-sate-wagyu.jpg',
        )];
        DB::table('foods')->insert($inserted);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods');
    }
}
