<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_paket');
            $table->string('harga_paket');
            $table->time('jam_buka_paket');
            $table->time('jam_tutup_paket');
            $table->string('keterangan_paket');
            // $table->unsignedBigInteger('makanan_id');
            // $table->string('namaMakanan');
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('makanan_id')->references('id')->on('foods');
        });

        $inserted = [array(
            'nama_paket' => 'Breakfast',
            'harga_paket' => '150000',
            'jam_buka_paket' => '06:00:00',
            'jam_tutup_paket' => '11:00:00',
            'keterangan_paket' => 'Tersedia Paket breakfast terdiri dari 1-5 meja (boleh request), menu utama sandwich, nasi goreng, ayam goreng, roti omelet, roti selai, minuman susu, teh, dll. bisa request menu makanan dan minuman lainnya',
            'image' => 'packagesDefault/one.jpg',
        ), array(
            'nama_paket' => 'Lunch',
            'harga_paket' => '180000',
            'jam_buka_paket' => '11:00:00',
            'jam_tutup_paket' => '17:00:00',
            'keterangan_paket' => 'Tersedia Paket lunch terdiri dari 1-5 meja (boleh request), menu utama  nasi sayur tempe, nasi ayam goreng/bakar, mie goreng/rebus, sop, soto, minuman es teh, jus, es jeruk, dll. bisa request menu makanan dan minuman lainnya',
            'image' => 'packagesDefault/two.jpg',
        ), array(
            'nama_paket' => 'Dinner',
            'harga_paket' => '200000',
            'jam_buka_paket' => '17:00:00',
            'jam_tutup_paket' => '20:00:00',
            'keterangan_paket' => 'Tersedia Paket dinner terdiri dari 1-5 kursi (boleh request), menu utama nasi sayur tempe, nasi ayam goreng/bakar, mie goreng/rebus, sop, soto, minuman es teh, jus, es jeruk, dll. bisa request menu makanan dan minuman lainnya',
            'image' => 'packagesDefault/three.jpg',
        ), array(
            'nama_paket' => 'Ultah',
            'harga_paket' => '500000',
            'jam_buka_paket' => '06:00:00',
            'jam_tutup_paket' => '20:00:00',
            'keterangan_paket' => 'Tersedia selama restoran buka. Paket ultah terdiri dari 8-16 meja (boleh request), menu utama cake ultah dan minuman jus, bisa request menu makanan dan minuman lainnya',
            'image' => 'packagesDefault/four.jpg',
        )];
        DB::table('packages')->insert($inserted);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
