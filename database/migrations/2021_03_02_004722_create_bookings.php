<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_booking');
            $table->string('nama_pemesan');
            $table->string('email');
            $table->string('telephone');
            $table->string('jumlah_pengunjung');
            $table->date('tanggal_pesan');
            $table->time('jam_pesan');
            $table->string('request_pesanan');
            $table->unsignedBigInteger('paket_id');
            $table->string('namaPaket');
            $table->string('status');
            $table->timestamps();

            $table->foreign('paket_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
