import axios from "axios";

export default function auth({
    next,
    router
}) {
    axios({
        method: "get",
        url: 'api/check-session',
        headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
            "Content-Type": "Application/JSON",
        },
    }).catch((err) => {
        if (err.response && err.response.status) {
            localStorage.clear();
            return router.push({
                name: 'login-admin'
            });
        }
    });
    if (!localStorage.getItem('token')) return router.push({
        name: 'home'
    });

    return next();
}