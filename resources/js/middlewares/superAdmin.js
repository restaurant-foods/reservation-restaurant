export default function auth({
    next,
    router
}) {
    if (localStorage.getItem('role') != 1) {
        return router.push({
            name: 'booking'
        });
    }
    return next();
}