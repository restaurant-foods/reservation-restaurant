/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

window.Vue = require('vue').default;

import App from './layouts/App.vue'
import router from './router';
import vuetify from './vuetify';


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.mixin({
    methods: {
        convertToRupiah(angka) {
            let prefix = "Rp. ";
            angka = angka.toString();
            var number_string = angka.replace(/[^,\d]/g, "").toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                var separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? prefix + rupiah : "";
        },
        uploadImage(index, type) {
            this.selectedImageType = type;
            this.$refs.attachment[index].$el.children[1].click();
        },

        onChangeUploadImage(e) {
            const image = e.dataUrl;
            const type = this.selectedImageType;
            if (!image) return;

            const fileBlob = this.dataURItoBlob(e.dataUrl, e.info.type);
            const file = new File([fileBlob], e.info.name, { type: fileBlob.type });

            if (type === "image") {
                this.urls.url_image = URL.createObjectURL(file);
                this.files.image = file;
            }
        },
        dataURItoBlob(dataURI, contentType) {
            let byteString;
            if (dataURI.split(",")[0].indexOf("base64") >= 0)
                byteString = atob(dataURI.split(",")[1]);
            else byteString = unescape(dataURI.split(",")[1]);

            // var mimeString = dataURI
            //   .split(',')[0]
            //   .split(':')[1]
            //   .split(';')[0];

            let ab = new ArrayBuffer(byteString.length);
            let ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            let bb = new Blob([ab], { type: contentType });
            return bb;
        },

        checkImage(model) {
            for (let url in this.urls) {
                if (url === `url_${model}` && this.urls[url] !== "") return true;
            }
        },

        showImage(model) {
            for (let url in this.urls) {
                if (url === `url_${model}`) return this.urls[url];
            }
        },

        validateImage() {
            this.imageErrors = [];

            for (let image in this.files) {
                if (this.files[image]) {
                    let fileName = this.files[image].name;
                    let extension = fileName.split(".").pop();

                    const standartExtensions = ["png", "jpg", "jpeg"];
                    const isRight = standartExtensions.filter(
                        ex => ex === extension.toLowerCase()
                    );

                    if (isRight.length <= 0) {
                        this.imageErrors.push({
                            model: image,
                            error: "Format file harus berupa .png / .jpg / .jpeg"
                        });
                    }
                }
            }
        },

        checkImageError(model) {
            let isError = this.imageErrors.filter(err => err.model === model);

            if (isError.length > 0) return true;
            else return false;
        },

        getImageError(model) {
            const error = this.imageErrors.filter(err => err.model === model);
            if (error.length <= 0) return;
            return error[0].error;
        },
    },
});

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    render: h => h(App)
});
