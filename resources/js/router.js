import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './views/Home.vue'
import Auth from './middlewares/auth'
import SuperAdmin from './middlewares/superAdmin'
import LoginAdmin from './views/admin/Login.vue'
import HomeAdmin from './views/admin/Home.vue'
import MakananPaket from './views/admin/MakananPaket.vue'
import Booking from './views/admin/Booking.vue'
import Reservation from './views/Reservation.vue'
import Page404 from './components/404.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/reservation/:id',
    name: 'Reservation',
    component: Reservation,
  },

  //Admin Panel
  {
    path: '/login',
    name: 'login-admin',
    component: LoginAdmin,
  },
  {
    path: '/home',
    name: 'home-admin',
    component: HomeAdmin,
    meta: {
      middleware: [Auth, SuperAdmin]
    }
  },
  {
    path: '/makanan-paket',
    name: 'makanan-paket',
    component: MakananPaket,
    meta: {
      middleware: [Auth, SuperAdmin]
    }
  },
  {
    path: '/booking',
    name: 'booking',
    component: Booking,
    meta: {
      middleware: Auth
    }
  },
  { path: "*", component: Page404 },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  if (!subsequentMiddleware) return context.next;
  return (...parameters) => {
    context.next(...parameters);
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({
      ...context,
      next: nextMiddleware
    });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware) ?
      to.meta.middleware : [to.meta.middleware];

    const context = {
      from,
      next,
      router,
      to
    };

    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({
      ...context,
      next: nextMiddleware
    });
  }
  return next();
});

export default router