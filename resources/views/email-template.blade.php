<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p style="font-family: 'Cutive Mono', monospace;">Hay {!! $namaPemesan !!},</p>
                    <p style="font-family: 'Cutive Mono', monospace;">Terimakasih,</p>
                    <p style="font-family: 'Cutive Mono', monospace;">Kami telah menerima pesanan anda dengan nomer pesanan <span style="color: mediumblue">{!! $kode !!}</span>    semoga hari anda meyenangkan dan selamat memesan kembali.</p>
                </div>
                <div class="card-body">
                    @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh mail has been sent to your email address.') }}
                    </div>
                    @endif
                    <div style="display: flex">

                        <div style="display: block; text-align: left;">
                    <hr style="  border:none; width:100%;
                    border-top:1px dotted rgb(0, 0, 0);
                    color:rgb(255, 255, 255);
                    background-color:rgb(255, 255, 255);
                    height:1px;">
                            <p style="font-family: 'Cutive Mono', monospace;">Nama kasir </p>
                            <p style="font-family: 'Cutive Mono', monospace;">Pesanan Anda </p>
                            <p style="font-family: 'Cutive Mono', monospace;">Paket </p>
                            <p style="font-family: 'Cutive Mono', monospace;">Menu Makanan </p>
                            <p style="font-family: 'Cutive Mono', monospace;">Total Harga </p>

                            <p style="font-family: 'Cutive Mono', monospace;">Keterangan </p>
                        </div>
                        <div style="display: block; text-align: right;"">
                            <p style="font-family: 'Cutive Mono', monospace;"> {!! $name !!}</p>
                            <p style="font-family: 'Cutive Mono', monospace;"> {!! $status !!}</p>
                            <p style="font-family: 'Cutive Mono', monospace;"> {!! $paket !!}</p>
                            <p style="font-family: 'Cutive Mono', monospace;"> {!! implode(", ",$makanan) !!}</p>
                            <p style="font-family: 'Cutive Mono', monospace;"> {!! number_format($total, 0, '', '.') !!}</p>   

                            <p style="font-family: 'Cutive Mono', monospace;"> {!! $content !!}</p>
                    <hr style="  border:none;
                    width:100%;
                    border-top:1px dotted rgb(0, 0, 0);
                    color:rgb(255, 255, 255);
                    background-color:rgb(255, 255, 255);
                    height:1px;">
                        <p style="font-family: 'Cutive Mono', monospace; color: darkorange; text-align: left;" >NB : Mohon disimpan, Karena Struk ini merupakan bukti booking yang sah</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>