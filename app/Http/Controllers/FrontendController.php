<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Food;
use App\Models\Package;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Models\FoodRelation;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller
{
    public function getPackages() {
        $packages = Package::get();

        foreach ($packages as $package) {
            $package->jam_buka_paket = Carbon::createFromFormat('H:i:s',$package->jam_buka_paket)->format('h:i');
            $package->jam_tutup_paket = Carbon::createFromFormat('H:i:s',$package->jam_tutup_paket)->format('h:i');

            $relations = FoodRelation::where('paket_id', $package->id)->get();
            $makanan_array = [];
            foreach ($relations as $relation) {
                $makanan = Food::where('id', $relation->makanan_id)->first();
                array_push($makanan_array, $makanan->nama_makanan);
            }
            $package->makanan = $makanan_array;
        }

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $packages
        ], 200);
    }

    public function detailPackage($id)
    {
        $package = Package::find($id);

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $package
        ], 200);
    }

    public function getFoods() {
        $foods = Food::get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $foods
        ], 200);
    }

    public function reservasiPaket(Request $request) {
        $validator = Validator::make($request->all(), [
            'kode_booking' => 'required|string',
            'nama_pemesan' => 'required|string',
            'email' => 'required|string',
            'telephone' => 'required|string',
            'jumlah_pengunjung' => 'required|string',
            'tanggal_pesan' => 'required|string',
            'jam_pesan' => 'required|string',
            'request_pesanan' => 'required|string',
            'paket_id' => 'required|integer',
            'namaPaket' => 'required|string',
            'status' => 'required|string',
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;
        $err = null;
        $success = false;

        $kode = date('m') . date('s') . "reservasi" . $request->paket . date('Y') . date('i');

        $item = new Booking;
        $item->kode_booking = $kode;
        $item->nama_pemesan = $request->get('nama_pemesan');
        $item->email = $request->get('email');
        $item->telephone = $request->get('telephone');
        $item->jumlah_pengunjung = $request->get('jumlah_pengunjung');
        $item->tanggal_pesan = $request->get('tanggal_pesan');
        $item->jam_pesan = $request->get('jam_pesan');
        $item->request_pesanan = $request->get('request_pesanan');
        $item->paket_id = $request->get('paket_id');
        $item->namaPaket = $request->get('namaPaket');
        $item->status = 'belum diterima';
        try {
            $item->save();
            $success = true;
            
        } catch (\Exception $e) {
            $err = $e->getMessage();
        }

        if ($success) {
            $status = "success";
            $message = "add booking successfully";
            $data = null;
            $code = 200;
        } else {
            $message = 'add booking failed';
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'error' => $err,
            'data' => null
        ], $code);
    }
}
