<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Food;
use App\Models\FoodRelation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::get();

        foreach ($packages as $package) {
            $package->jam_buka_paket = Carbon::createFromFormat('H:i:s',$package->jam_buka_paket)->format('h:i');
            $package->jam_tutup_paket = Carbon::createFromFormat('H:i:s',$package->jam_tutup_paket)->format('h:i');

            $relations = FoodRelation::where('paket_id', $package->id)->get();
            $makanan_array = [];
            $makanan_id_array = [];
            foreach ($relations as $relation) {
                $makanan = Food::where('id', $relation->makanan_id)->first();
                array_push($makanan_array,$makanan->nama_makanan);
                array_push($makanan_id_array,$makanan->id);
            }
            $package->makanan = $makanan_array;
            $package->makanan_id = $makanan_id_array;
        }

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $packages
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_paket' => 'required|string',
            'harga_paket' => 'required|integer',
            'jam_buka_paket' => 'required|string',
            'jam_tutup_paket' => 'required|string',
            'keterangan_paket' => 'required|string',
            'makanan_id' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif|required'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $package = new Package;
        
            // $item->item_category_id = $request->get('item_category_id');
            $package->nama_paket = $request->get('nama_paket');
            $package->harga_paket = $request->get('harga_paket');
            $package->jam_buka_paket = $request->get('jam_buka_paket');
            $package->jam_tutup_paket = $request->get('jam_tutup_paket');
            $package->keterangan_paket = $request->get('keterangan_paket');
            // $package->makanan_id = $request->get('makanan_id');
            // $package->namaMakanan = $request->get('namaMakanan');
            $package->image = $request->file('image')->store('packages', 'public');
    
            $package->save();

            if ($package) {
                $json_decode = $request->get('makanan_id');
                $makanan_id = json_decode($json_decode);
                foreach ($makanan_id as  $value) {
                    $relation = new FoodRelation;
                    $relation->paket_id = $package->id;
                    $relation->makanan_id = $value;
                    $relation->save();
                }
                $status = "success";
                $message = "add package successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'add package failed';
            }
        }

        // if ($package) {
        //     $json_decode = $request->get('makanan_id');
        //     $makanan_id = json_decode($json_decode);
        //     foreach ($makanan_id as  $value) {
        //         $relation = new FoodRelation;
        //         $relation->paket_id = $package->id;
        //         $relation->makanan_id = $value;
        //         $relation->save();
        //     }
        //     $status = "success";
        //     $message = "add package successfully";
        //     $data = null;
        //     $code = 200;
        // } else {
        //     $message = 'add package failed';
        // }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $package
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_paket' => 'required|string',
            'harga_paket' => 'required|integer',
            'jam_buka_paket' => 'required|string',
            'jam_tutup_paket' => 'required|string',
            'keterangan_paket' => 'required|string',
            'makanan_id' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $package = Package::findOrFail($id);
            $package->nama_paket = $request->get('nama_paket');
            $package->harga_paket = $request->get('harga_paket');
            $package->jam_buka_paket = $request->get('jam_buka_paket');
            $package->jam_tutup_paket = $request->get('jam_tutup_paket');
            $package->keterangan_paket = $request->get('keterangan_paket');

            if ($request->file('image')) {
                if ($package->image && file_exists(storage_path('app/public/' . $package->image))) {
                    Storage::delete('public/' . $package->image);
                }

                $package->image = $request->file('image')->store('packages', 'public');
            }
            
            $package->save();

            if ($package) {
                FoodRelation::where('paket_id', $package->id)->delete();
                $json_decode = $request->get('makanan_id');
                $makanan_id = json_decode($json_decode);
                foreach ($makanan_id as  $value) {
                    $relation = new FoodRelation;
                    $relation->paket_id = $package->id;
                    $relation->makanan_id = $value;
                    $relation->save();
                }
                $status = "success";
                $message = "update product successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update product failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FoodRelation::where('paket_id', $id)->delete();
        $package = Package::findOrFail($id);

        if ($package->image && file_exists(storage_path('app/public/' . $package->image))) {
            Storage::delete('public/' . $package->image);
        }
        
        $package->delete();
        
        return response()->json([
            'status' => 'success',
            'message' => 'delete package successfully',
            'data' => null
        ], 200);
    }
}
