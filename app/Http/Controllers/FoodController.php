<?php

namespace App\Http\Controllers;

use App\Models\Food;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FoodRelation;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foods = Food::get();

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $foods
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_makanan' => 'required|string',
            'harga_makanan' => 'required|integer',
            'keterangan_makanan' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif|required'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        $food = new Food;
        $food->nama_makanan = $request->get('nama_makanan');
        $food->harga_makanan = $request->get('harga_makanan');
        $food->keterangan_makanan = $request->get('keterangan_makanan');
        $food->image = $request->file('image')->store('foods', 'public');

        $food->save();

        if ($food) {
            $status = "success";
            $message = "add food successfully";
            $data = null;
            $code = 200;
        } else {
            $message = 'add food failed';
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_makanan' => 'required|string',
            'harga_makanan' => 'required|integer',
            'keterangan_makanan' => 'required|string',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else {
            $food = Food::findOrFail($id);
            $food->nama_makanan = $request->get('nama_makanan');
            $food->harga_makanan = $request->get('harga_makanan');
            $food->keterangan_makanan = $request->get('keterangan_makanan');

            if ($request->file('image')) {
                if ($food->image && file_exists(storage_path('app/public/' . $food->image))) {
                    Storage::delete('public/' . $food->image);
                }

                $food->image = $request->file('image')->store('foods', 'public');
            }
            
            $food->save();

            if ($food) {
                $status = "success";
                $message = "update food successfully";
                $data = null;
                $code = 200;
            } else {
                $message = 'update food failed';
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FoodRelation::where('makanan_id', $id)->delete();
        $food = Food::findOrFail($id);

        if ($food->image && file_exists(storage_path('app/public/' . $food->image))) {
            Storage::delete('public/' . $food->image);
        }
        
        $food->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'delete food successfully',
            'data' => null
        ], 200);
    }
}
