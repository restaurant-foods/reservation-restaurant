<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
  
        $status = "error";
        $message = "";
        $data = null;
        $code = 302;

        $user = User::where('email', '=', $request->get('email'))->first();
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = $errors;
        } else if ($user) {
            if(Hash::check($request->get('password'), $user->password)) {
                $user->generateToken();

                $status = 'success';
                $message = 'Login sukses';
                $data = $user;
                $code = 200;
            } else {
                $message = "Login gagal, password salah";
            }
        } else {
            $message = "Login gagal, user " . $request->email . " tidak ditemukan";
        }
  
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function logout(Request $request)
    {
        $user = auth('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'logout berhasil',
            'data' => null
        ], 200);
    }
}
