<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Dotenv\Validator;
use App\Models\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Food;
use App\Models\FoodRelation;
use App\Models\Package;
use Illuminate\Support\Facades\Mail;
use PDF;

// use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::orderBy('id', 'DESC')->get();

        foreach ($bookings as $booking) {
            $booking->jam_pesan = Carbon::createFromFormat('H:i:s', $booking->jam_pesan)->format('h:i');
        }

        return response()->json([
            'status' => 'success',
            'message' => null,
            'data' => $bookings
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'status' => 'required|string',
        // ]);

        $status = "error";
        $message = "";
        $data = null;
        $code = 400;

        $booking = Booking::findOrFail($id);
        $booking->status = $request->get('status');

        $booking->save();

        if ($booking) {
            $package = Package::findOrFail($request->paket_id);
            $relations = FoodRelation::where('paket_id', $package->id)->get();
            $makanan_array = [];
            foreach ($relations as $relation) {
                $makanan = Food::where('id', $relation->makanan_id)->first();
                array_push($makanan_array,$makanan->nama_makanan);
            }
            $total = $package->harga_paket * $booking->jumlah_pengunjung;
            $data = [
                'subject' => 'Bukti Booking Reservasi',
                'name' => $request->name,
                'email' => $request->email,
                'status' => $request->status,
                'kode' => $booking->kode_booking,
                'namaPemesan' => $booking->nama_pemesan,
                'paket' => $package->nama_paket,
                'makanan' => $makanan_array,
                'total' => $total,
                'content' => $request->content
            ];

            $pdf = PDF::loadView('email-template', $data);

            Mail::send('email-template', $data, function ($message) use ($data, $pdf) {
                $message->to($data['email'])
                    ->subject($data['subject'])
                    ->attachData($pdf->output(), "reservasi.pdf");
            });

            if (Mail::failures()) {
                foreach (Mail::failures() as $email_address) {
                    $message += " Mail failure" . $email_address;
                }
            }
            $status = "success";
            $message = "update booking successfully";
            $data = null;
            $code = 200;
        } else {
            $message = 'update booking failed';
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => null
        ], $code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
