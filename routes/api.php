<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [UserController::class, 'login']);

Route::middleware('auth:api')->group(function() {
    Route::post('/logout', [UserController::class, 'logout']);
    Route::get('/check-session', function() { return true; });
    // Route::resource('pakets', PaketController::class);
    // Route::resource('foods', FoodController::class);
    Route::resource('bookings', BookingController::class);
});

Route::middleware(['auth:api','superadmin'])->group(function() {
    Route::resource('pakets', PaketController::class);
    Route::resource('foods', FoodController::class);
});

Route::get('/all-packages', [FrontendController::class, 'getPackages']);
Route::get('/detail-package/{id}', [FrontendController::class, 'detailPackage']);
Route::get('/all-foods', [FrontendController::class, 'getFoods']);
Route::post('/reservasi', [FrontendController::class, 'reservasiPaket']);